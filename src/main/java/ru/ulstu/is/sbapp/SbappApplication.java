package ru.ulstu.is.sbapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;
import java.util.Objects;

@SpringBootApplication
@RestController
public class SbappApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbappApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name, @RequestParam(value = "filter", defaultValue = "lower") String filter) {
		return String.format("Hello %s", filter(name, filter));
	}

	private String filter(String value, String filter) {
		if (Objects.equals(filter, "lower")) {
			return value.toLowerCase(Locale.ROOT);
		}
		return value.toUpperCase(Locale.ROOT);
	}
}
